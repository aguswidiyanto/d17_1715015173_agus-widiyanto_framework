<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->insert([
        	[
        		'username'=>'AGUS', 'email'=>'AGUS@GMAIL.COM',
            	'address'=>'JL.KEMAKMURAN RT.9', 'phone'=>'081352523449',
            	'status'=>'ACTIVE', 'avatar'=>'0LmYYVCXeYjAjoi1chtrmlnIKUnbXQkoV1iCvl5Y.jpeg', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'AMING', 'email'=>'AMING@GMAIL.COM',
            	'address'=>'JL.CENDANA RT.9', 'phone'=>'081352523236',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123457")
        	],
        	[
        		'username'=>'AHMAD', 'email'=>'AHMAD@GMAIL.COM',
            	'address'=>'JL.KEMERDEKAAN RT.9', 'phone'=>'081352523099',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANDRA', 'email'=>'ANDRA@GMAIL.COM',
            	'address'=>'JL.GERILYA RT.9', 'phone'=>'081352523875',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANNA', 'email'=>'ANNA@GMAIL.COM',
            	'address'=>'JL.SENTOSA RT.9', 'phone'=>'081352523555',
            	'status'=>'INACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ALESA', 'email'=>'ALESA@GMAIL.COM',
            	'address'=>'JL.KENANGAN RT.9', 'phone'=>'081352523765',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ALENA', 'email'=>'ALENA@GMAIL.COM',
            	'address'=>'JL.ABUL HASAN RT.9', 'phone'=>'081352523666',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ALDI', 'email'=>'ALDI@GMAIL.COM',
            	'address'=>'JL.AHMAD DAHLAN RT.9', 'phone'=>'081352523567',
            	'status'=>'INACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANDI', 'email'=>'ANDI@GMAIL.COM',
            	'address'=>'JL.JUANDA RT.9', 'phone'=>'081352523673',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ARI', 'email'=>'ARI@GMAIL.COM',
            	'address'=>'JL.SEMPAJA RT.9', 'phone'=>'081352523786',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANDA', 'email'=>'ANDA@GMAIL.COM',
            	'address'=>'JL.WAHAB SYAHRANIE RT.9', 'phone'=>'081352523090',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ALDA', 'email'=>'ALDA@GMAIL.COM',
            	'address'=>'JL.BUNG TOMO RT.9', 'phone'=>'081352523324',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANTO', 'email'=>'ANTO@GMAIL.COM',
            	'address'=>'JL.S.KLEDANG RT.9', 'phone'=>'08135252345',
            	'status'=>'INACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ANWAR', 'email'=>'ANWAR@GMAIL.COM',
            	'address'=>'JL.KADRIE OENING RT.9', 'phone'=>'081352523122',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        	[
        		'username'=>'ARTA', 'email'=>'ARTA@GMAIL.COM',
            	'address'=>'JL.SLAMET RIYADI RT.9', 'phone'=>'081352522367',
            	'status'=>'ACTIVE', 'avatar'=>'', 'password'=>Hash::make("123456")
        	],
        ]);
    }
}
