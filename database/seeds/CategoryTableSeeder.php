<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CategoryTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('categories')->insert([
        	[
        		'category_name'=>'MetroPop',
        		'description'=>'Ketika Rachel Chu, dosen ekonomi keturunan Cina, setuju untuk pergi
				ke Singapura bersama kekasihnya, Nick, ia membayangkan
				rumah sederhana, jalan-jalan keliling pulau, dan menghabiskan waktu
				bersama pria yang mungkin akan menikah dengannya itu.'
        	],
        	[
            	'categories_name'=>'SCI-FI',
            	'description'=>'Kesulitan datang dari makhluk yang kecil. Yang ini tingginya satu setengah meter dan beratnya 44 kg. Namanya Serenity Harper, ia gadis yang sangat menyebalkan, mau menang sendiri, pemarah dan egois. Dapatkah sikap penuh perhatian dan kasih sayang dari Derek, Kimberly, dan para anggota Klub Doa meluluhkan hati Serenity � dan membuktikan bahwa cinta sejati itu benar-benar ada?'
	        ],
	        [
	            'categories_name'=>'HOROR',
	            'description'=>'Apa yang paling kau takutkan ketika gelap?
				Pernahkah kau mendengar mitos tentang pesan misterius yang muncul ketika kau terjebak sendirian di rumah saat lampu padam?
				Konon, �si pengirim pesan� akan menerormu tanpa ampun dan kau akan terjebak dalam permainan itu selamanya. Apa ada kesempatan untuk lolos dari teror itu? Entahlah, tapi sebaiknya kau tidak meremehkan mitos ini. 
				'
	        ],
	        [
	            'categories_name'=>'FANTASI',
	            'description'=>'Menjadi Harry Potter memang sulit dan sekarang pun tidak lebih mudah ketika ia menjadi pegawai Kementerian Sihir yang kelelahan, suami, dan ayah tiga anak usia sekolah.
				Sementara Harry berjuang menghadapi masa lalu yang mengikutinya, putra bungsunya, Albus, harus berjuang menghadapi beban warisan keluarga yang tak pernah ia inginkan. Ketika masa lalu dan masa sekarang melebur, ayah dan anak pun mengetahui fakta yang tidak menyenangkan: terkadang kegelapan datang dari tempat-tempat yang tak terduga. Berdasarkan cerita asli baru karya J.K. Rowling, John Tiffany, dan Jack Thorne, naskah untuk Harry Potter dan si Anak Terkutuk aslinya dirilis sebagai "edisi latihan khusus" bersama pementasan perdana di West End London pada musim panas 2016.'
	        ],
	        [
	            'categories_name'=>'ROMANCE',
	            'description'=>'Ayat Ayat Cinta 2 ini adalah karya sastra racikan Kang Abik yang mengejutkan. Lebih bcrani dan dinamis. Tapi tetap sarat makna dan pesan.
				Melly Goeslaw, Musisi Indonesia. Jika Sutarji Calzoum Bahri selalu menyihir dengan puisi-puisi mantranya, maka Habiburrahman El Shirazy selalu menyihir dengan novel-novel pembangun jiwanya. Sungguh, Ayat Ayat Cinta 2 ini menyihir dan menggetarkan jiwa dari awal sampai
				akhir.'
	        ],
	        [
	            'categories_name'=>'FANFICTION',
	            'description'=>'Gereja akhirnya mempertemukan kembali Alwin dan Liana yang berpisah sejak lulus SMA. Liana yang ternyata Xowner, akhirnya harus ikut andil dalam masalah yang ada di dalam boyband XO-IX. Liana membongkar sebuah rahasia tentang Agoy, yang tidak pernah diketahui oleh personil XO-IX lainnya. Dan hal itu pula yang membuat Liana bertemu dengan sahabat masa kecilnya, Yocky.'
	        ],
	        [
	            'categories_name'=>'HUMOR',
	            'description'=>'KAMBINGJANTAN adalah kumpulan cerita sehari-hari yang konyol dan unik dari kehidupan Raditya Dika (Radit), mahasiswa hasil peranakan orang Batak dengan mesin jahit, format yang ditampilkan adalah format diary, dan buku ini adalah kumpulan dari diary dia.'
	        ],
	        [
	            'categories_name'=>'MISTERI',
	            'description'=>'tokoh detektif fiksi rekaan Sir Arthur Conan Doyle, seorang pengarang dan dokter berkebangsaan Skotlandia. Holmes yang menyebut dirinya sebagai seorang "detektif konsultan" ini dikenal akan ketajaman penalaran logis, kemampuan menyamar, dan keterampilannya dalam menggunakan ilmu forensik untuk memecahkan berbagai kasus'
	        ],
	        [
	            'categories_name'=>'HISTORICAL FICTION',
	            'description'=>'novel pertama dari rangkaian trilogi The Search For Merlin. Buku keduanya sedang dalam tahap penulisan, judulnya The Grey Labyrinth. '
	        ],
	        [
	            'categories_name'=>'ADVENTURE',
	            'description'=>'Lima sekawan adalah Julian, Dick, George, Anne dan - tentu saja - Timmy si anjing! Kemanapun mereka pergi pasti ada petualangan yang seru dan mengasyikkan! Bagaimana mereka memecahkan misteri kali ini?!? Petulangan KEDUA mereka yang seru dan mengasyikkan: Pencuri di Pondok Kirrin!Siapakah orangnya?Rasanya Lima Sekawan tahu... tapi mereka harus punya…'
			],
			[
	            'categories_name'=>'Sci-Fi',
	            'description'=>'Tere: 17 tahun, 2 SMU, wajah manis. Saat jalan-jalan di mal, dia berebut kaus dengan cowok cute tapi nyebelin. Besoknya di sekolah, Tere ketemu lagi sama cowok itu. Ternyata cowok itu anak baru, namanya Giovani.'
	        ],
	        [
	            'categories_name'=>'TEENLIT',
	            'description'=>'Mia, gadis 17 tahun penggemar musik klasik yang mengalami koma dan ditinggal oleh orang-orang yang Ia sayang. Mia rasanya ingin mati saja, sampai Adam datang menjenguknya. Cinta pertama Mia yang juga musisi rock itu ingin Mia tetap tinggal dan bertahan'
	        ],
	        [
	            'categories_name'=>'CHICK-LIT',
	            'description'=>'Tess Parks, perempuan muda yang nggak percaya dengan cinta, sehingga dia selalu berjanji pada dirinya sendiri untuk memutuskan hubungannya dengan pria setelah berpacaran selama 6 minggu. Ketika musim panas datang, teman dekatnya memintanya untuk menemani liburan di Corfu, Yunani.'
	        ],
	        [
	            'categories_name'=>'ROMANCE',
	            'description'=>'ILY FROM 38.000 FEET adalah film yang menggambarkan sebuah percintaan antara rizky nazar dan michele zuidith'
	        ],
	        [
	            'categories_name'=>'ROMANCE',
	            'description'=>'zainuddin yang mengejar cinta dari hayati'
	        ],
        ]);
    }
}
